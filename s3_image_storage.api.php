<?php

/**
 * @file
 * Hooks related to image styles and effects.
 */

/**
 * React to the storage of a new derivative in the local database.
 *
 * This hook enables other modules to update existing records with the
 * S3 URL. At this point, the image is already uploaded to S3.
 *
 * @param object $record Database recording containing the record id.
 */
function hook_s3_image_storage_new_record($record) {
  // Determine the name of the field table that this record uses.
  // Can be used to load the id of the entity that this field uses.
  $field_name = $record->field_name;
  $field = field_info_field($field_name);
  $field_current_tbl_name = array_keys($field['storage']['details']['sql']['FIELD_LOAD_CURRENT']);
}