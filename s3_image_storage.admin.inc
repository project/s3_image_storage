<?php

/**
 * Form callback to configure global settings for this module.
 */
function s3_image_storage_form_configuration() {
  // Add CSS.
  drupal_add_css(drupal_get_path('module', 's3_image_storage') . '/css/s3_image_storage.admin.css');

  // Define form array.
  $form = array();

  $form['readme'] = array(
    '#type' => 'fieldset',
    '#title' => 'General Information',
  );

  $form['readme']['description'] = array(
    '#markup' => t('To enable S3 images uploads for content types, navigate to the field itself
      to set the image styles that should be generated for an image upload. Your S3 settings
      need to be valid, otherwise this module with trigger errors.'),
  );

  // Add S3 settings.
  _s3_image_storage_form_configuration_storage($form);

  // Add file handling settings.
  _s3_image_storage_form_configuration_file_handling($form);

  // Add async processing settings.
  _s3_image_storage_form_configuration_async($form);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Settings form helper function: S3 Settings.
 */
function _s3_image_storage_form_configuration_storage(&$form) {
  // Determine if all information is available in order to test the connection.
  $s3_connection_props = array(
    'access_key' => variable_get('s3_image_storage_s3_bucket_access_key_id', ''),
    'secret_access_key' => variable_get('s3_image_storage_s3_bucket_secret_access_key', ''),
    'storage_region' => variable_get('s3_image_storage_storage_region', 'us-east-1'),
    'storage_acl' => variable_get('s3_image_storage_storage_acl', 'public-read'),
    'bucket_name' => variable_get('s3_image_storage_storage_bucket_name', ''),
  );

  // Determine S3 connection properties.
  $props_available = true;
  array_walk($s3_connection_props, function($value) use (&$props_available) {
    if (empty($value)) {
      $props_available = false;
    }
  });

  // Store metadata in form.
  $form['metadata'] = array(
    '#type' => 'value',
    '#value' => array(
      'storage_region' => $s3_connection_props['storage_region'],
    ),
  );

  $form['storage_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'S3 Settings',
  );

  $description_markup_addtl = (!$props_available) ? t('Not all configuration elements are available to
    the the connection with S3.') : t('Click below to test the connection with S3.');
  $form['storage_settings']['test_connection_description'] = array(
    '#type' => 'item',
    '#title' => t('Test Current S3 Connection Settings'),
    '#markup' => t('The credentials required to connect to AWS should be stored in your settings.php
    file. Use the key <em>s3_image_storage_s3_bucket_access_key_id</em> for the access key and the key
    <em>s3_image_storage_s3_bucket_secret_access_key</em> for the secret access key. ') . $description_markup_addtl,
  );

  $form['storage_settings']['test_connection'] = array(
    '#type' => 'submit',
    '#value' => t('Test Connection'),
    '#disabled' => (!$props_available),
    '#submit' => array('s3_image_storage_form_configure_storage_test_connection'),
  );

  // Access Key.
  if (empty($s3_connection_props['access_key'])) {
    $access_key_text = t('<em class="error">Error: Your access key was not found</em>. Please add it to your settings file.');
  } else {
    $access_key_text = t('We found an access key in your settings file ending in <em>!key_end</em>.', array(
      '!key_end' => substr($s3_connection_props['access_key'], -4, 4),
    ));
  }
  $form['storage_settings']['access_key'] = array(
    '#type' => 'item',
    '#title' => 'Access Key',
    '#markup' => $access_key_text,
  );

  // Secret Access Key.
  if (empty($s3_connection_props['secret_access_key'])) {
    $secret_access_key_text = t('<em class="error">Error: Your secret access key was not found</em>. Please add it to your settings file.');
  } else {
    $secret_access_key_text = t('We found a secret access key in your settings file ending in <em>!key_end</em>.', array(
      '!key_end' => substr($s3_connection_props['secret_access_key'], -4, 4),
    ));
  }
  $form['storage_settings']['secret_access_key'] = array(
    '#type' => 'item',
    '#title' => 'Secret Access Key',
    '#markup' => $secret_access_key_text,
  );

  // S3 Region.
  $form['storage_settings']['region'] = array(
    '#type' => 'select',
    '#title' => t('Region'),
    '#options' => array(
      "ap-northeast-1" => "Asia Pacific (Tokyo)",
      "ap-southeast-2" => "Asia Pacific (Sydney)",
      "ap-southeast-1" => "Asia Pacific (Singapore)",
      "cn-north-1" => "China (Beijing)",
      "eu-central-1" => "EU (Frankfurt)",
      "eu-west-1" => "EU (Ireland)",
      "us-east-1" => "US East (N. Virginia)",
      "us-west-1" => "US West (N. California)",
      "us-west-2" => "US West (Oregon)",
      "sa-east-1" => "South America (Sao Paulo)",
    ),
    '#default_value' => $s3_connection_props['storage_region'],
    '#description' => t('Select the region of your S3 bucket.'),
  );

  // S3 ACL.
  $canned_acls = array('private', 'public-read', 'public-read-write', 'aws-exec-read', 'authenticated-read', 'bucket-owner-read', 'bucket-owner-full-control', 'log-delivery-write');
  $form['storage_settings']['acl'] = array(
    '#type' => 'select',
    '#title' => t('ACL (Access Control List)'),
    '#options' => drupal_map_assoc($canned_acls),
    '#default_value' => $s3_connection_props['storage_acl'],
    '#description' => t('Select the ACL for uploaded files. Currently only supports canned ACLs. See !doclink for more details.', array(
      '!doclink' => l('documentation', 'http://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html', array(
        'attributes' => array('target' => '_blank'),
      )),
    )),
  );

  // Bucket name.
  $form['storage_settings']['bucket_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Bucket Name'),
    '#default_value' => $s3_connection_props['bucket_name'],
    '#maxlength' => 63,
    '#description' => t('Enter the bucket name that should be used for image storage. Must be at least 3 characters, maximum of 63 characters.'),
    '#required' => true,
  );
}

/**
 *
 */
function _s3_image_storage_form_configuration_file_handling(&$form) {
  $form['file_handling_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'File Handling',
    '#description' => t('<b>Warning:</b> While it is possible to change these settings at any time, they should not be changed
      after this module starts working. Doing so might have unintended side-effects.')
  );

  $form['file_handling_fieldset']['preserve_local_derivatives'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve all local image derivatives when local file gets uploaded'),
    '#description' => t('By default, locally generated image derivatives are deleted after they get uploaded to S3.
      This will not prevent Drupal from generating derivatives locally whenever a derivative is displayed locally.
      Checking this box will preserve any image derivatives on the local filesystem.'),
    '#default_value' => variable_get('s3_image_storage_preserve_originals', 0),
  );

  $form['file_handling_fieldset']['preserve_remote_derivatives'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve remote image derivatives when local file gets deleted'),
    '#description' => t('By default, remote image derivatives are deleted when the local file is deleted.
      Check this box to preserve any generated derivatives in your S3 bucket when the related local file gets deleted.
      Checking this box will also preserve any local database records for files that are preserved.
      This settings does not apply to the original file that gets uploaded to your S3 bucket. Checking this box will not delete S3 related
      to local files that have been deleted in the past. Unchecking this box will not recover previously deleted S3 images.
      This setting does not apply to the remote original image.'),
    '#default_value' => variable_get('s3_image_storage_preserve_remote_derivatives', 0),
  );

  $form['file_handling_fieldset']['preserve_remote_original'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve remote image original when local file gets deleted'),
    '#description' => t('By default, the remote image original is deleted when the local file is deleted.
      Check this box to preserve the remote original imagein your S3 bucket when the related local file gets deleted.
      Checking this box will also preserve any local database records for files that are preserved.
      This settings does not apply to the remote image derivatives file that gets uploaded to your S3 bucket. Checking this box will not delete S3 related
      to local files that have been deleted in the past. Unchecking this box will not recover previously deleted S3 images.
      This setting does not apply to any remote derivative image.'),
    '#default_value' => variable_get('s3_image_storage_preserve_remote_original', 0),
  );
}

/**
 * Helper functions for async settings.
 */
function _s3_image_storage_form_configuration_async(&$form) {
  $form['async'] = array();

  $form['async'] = array(
    '#type' => 'fieldset',
    '#title' => 'Async Settings',
    '#description' => t('Configure settings that control how queues are
      triggered asynchronously after an image is uploaded.')
  );

  // Defines method to for asynchronous queue triggering.
  $form['async']['async_method'] = array(
    '#type' => 'select',
    '#title' => t('Method'),
    '#description' => t('Select your preferred method for creating the async
      connection. To use Curl, you need to have the !curl extension installed,
      which you !curl_detection.', array(
      '!curl' => l('Curl', 'http://php.net/manual/en/book.curl.php', array(
        'attributes' => array('target' => '_blank'),
      )),
      '!curl_detection' => (function_exists('curl_version')) ? 'do' : 'do not',
    )),
    '#options' => array(
      "fsockopen" => t('Socket Connection (fsockopen)'),
      "curl" => t('Curl'),
    ),
    '#default_value' => variable_get('s3_image_storage_async_method', 'fsockopen'),
  );

  // Defines timeout.
  $form['async']['async_timeout'] = array(
    '#type' => 'select',
    '#title' => t('Method'),
    '#description' => t('Select how many milliseconds the asynchronous call
      should wait before it closes the connection. This time will be added to
      the time it takes to upload an image file if "queue and process
      asynchronously" is the method you have selected for any image fields.
      The asynchronous processing does not seem to work if the connection
      terminates right away. If processing asynchronously does not work,
      increase this value.'),
    '#options' => array(
      50 =>  '50 ms',
      100 => '100 ms',
      200 => '200 ms',
      300 => '300 ms',
      400 => '400 ms',
      500 => '500 ms',
      600 => '600 ms',
      700 => '700 ms',
      800 => '800 ms',
      900 => '900 ms',
      1000 => '1000 ms',
    ),
    '#default_value' => variable_get('s3_image_storage_async_timeout', 100),
  );
}

/**
 * Submit callback for image field configuration form.
 */
function s3_image_storage_form_configuration_submit($form, &$form_state) {
  // Store file handling flags.
  variable_set('s3_image_storage_preserve_originals', $form_state['values']['preserve_local_derivatives']);

  // Store remote derivatives deletion flag.
  variable_set('s3_image_storage_preserve_remote_derivatives', $form_state['values']['preserve_remote_derivatives']);

  // Store remote original deletion flag.
  variable_set('s3_image_storage_preserve_remote_original', $form_state['values']['preserve_remote_original']);

  // Store region.
  variable_set('s3_image_storage_storage_region', $form_state['values']['region']);

  // Store S3 ACL.
  variable_set('s3_image_storage_storage_acl', $form_state['values']['acl']);

  // Store bucket name.
  variable_set('s3_image_storage_storage_bucket_name', $form_state['values']['bucket_name']);

  // Store async method and timeout.
  variable_set('s3_image_storage_async_method', $form_state['values']['async_method']);
  variable_set('s3_image_storage_async_timeout', $form_state['values']['async_timeout']);

  // Add success message.
  drupal_set_message('S3 settings were successfully saved. Click "Test Connection" to test your connection.');
}

/**
 * Test the S3 connection.
 */
function s3_image_storage_form_configure_storage_test_connection($form, &$form_state) {
  // Connect to bucket and get the policy.
  $s3Obj = new S3ImageStorageS3();
  try {
    $result = $s3Obj->getBucketLocation();
    $locationConstraint = $result->get('LocationConstraint');

    // Verify that the set region matches with the region of this S3 bucket.
    if ($locationConstraint != $form['metadata']['#value']['storage_region']) {
      drupal_set_message(t('The connection test was not successful. Check your settings.'), 'error');
    } else {
      drupal_set_message(t('The connection was successfully tested.'));
    }
  } catch (Exception $e) {
    drupal_set_message(t('The connection test was not successful. Check your settings.<br />@message', array(
      '@message' => $e->getMessage(),
    )), 'error');
  }
}
