<?php

/**
 * Define custom exception class for image processing exception.
 */
class S3ImageStorageImageProcessorException extends Exception{}

/**
 * Image processing class.
 */
class S3ImageStorageImageProcessor {
  /**
   * @var array $styles
   *    Image styles that need processing.
   */
  private $styles;

  /**
   * @var int $fid File Id.
   */
  private $fid;

  /**
   * @var string $uri
   *    URI of the original image.
   */
  private $uri;

  /**
   * @var string $fieldName Name of origin field.
   */
  private $fieldName;

  /**
   * @var obj $s3Client
   *   S3 Client.
   */
  private $s3Client;

  /**
   * @var string $baseDirectoryToken Token for base directory.
   *   Normally defined in image field.
   */
  private $baseDirectoryToken = '';

  /**
   * @var string $baseDirectory Base directory for image uploads.
   */
  private $baseDirectory = '';


  /**
   * Constructor.
   *
   * @param array $args Array of parameters including:
   *   - (int)      fid         File Id
   *   - (string)   uri         URI
   *   - (array)    styles      Array of style names
   *   - (string)   field_name  name Field Name
   *   - (array)    regenerate  Metadata containing information on regenerating
   *                            derivatives.
   *
   * @throws Exception if uri is not a a string.
   */
  public function __construct($args = array()) {
    // Verify format for file ID.
    if (!is_numeric($args['fid'])) {
      throw new S3ImageStorageImageProcessorException('File ID needs to be an integer.');
    }

    // Verify format for URI.
    if (!is_string($args['uri'])) {
      throw new S3ImageStorageImageProcessorException('URI needs to be a string.');
    }

    // Verify file exists.
    if (!file_exists($args['uri'])) {
      throw new S3ImageStorageImageProcessorException(sprintf('Source file %s does not exist.', $args['uri']), 520);
    }

    // Verify base directory token is set.
    if (empty($args['base_directory_token'])) {
      throw new S3ImageStorageImageProcessorException(sprintf('Base directory cannot be empty.'));
    }

    // Verify field name is set.
    if (!is_string($args['field_name']) || empty($args['field_name'])) {
      throw new S3ImageStorageImageProcessorException(sprintf('Field name cannot be empty.'));
    }

    // If files should be regenerated, verify that base key is
    // set in regenerate array.
    if (!empty($args['regenerate_data']) && (
        !is_array($args['regenerate_data']) || !array_key_exists('base_directory', $args['regenerate_data']))
    ) {
      throw new S3ImageStorageImageProcessorException(sprintf('Metadata passed to regenerate
        image is not sufficient.'));
    }

    // Set arguments as class properties.
    $this->fid = $args['fid'];
    $this->uri = $args['uri'];
    $this->styles = $args['styles'];
    $this->fieldName = $args['field_name'];
    $this->baseDirectoryToken = $args['base_directory_token'];

    // Determine base directory if not passed as metadata.
    if (empty($args['regenerate_data'])) {
      $this->determineBaseDirectory();
    } else {
      $this->baseDirectory = $args['regenerate_data']['base_directory'];
    }
  }

  /**
   * Generate all derivatives for given styles.
   */
  public function generateDerivatives() {
    foreach ($this->styles as $style_name) {
      $this->generateDerivative($style_name);
    }

    // Log message.
    watchdog('s3_image_storage', 'Successfully generated all image derivatives for file id !fid using the URI !uri.', array(
      '!fid' => $this->fid,
      '!uri' => $this->uri,
    ));
  }
  /**
   * Upload original file.
   */
  public function uploadOriginal() {
    $this->syncFile($this->uri, $this->determineObjectKey('original'), true);
  }

  /**
   * Sync file to permanent storage system (S3).
   *
   * @param string $uri URI of photo file.
   * @param string $objectKey S3 object key. If key is empty, S3ImageStorageS3
   *   will automatically generate a directory and use the following name:
   *   origin.[mime-type].
   * @param boolean $original Flag indicating that this is the original image.
   */
  private function syncFile($uri, $objectKey = '', $original = false) {
    // Initialize S3 client.
    $this->s3Client = new S3ImageStorageS3Upload($uri, $objectKey);

    // Upload file.
    $resultObj = $this->s3Client->uploadFile();

    // Write a reference to this object to the local database.
    $record = new stdClass();
    $record->fid = $this->fid;
    $record->object_key = $objectKey;
    $record->bucket_name = $this->s3Client->getS3Props('bucketName');
    $record->field_name = $this->fieldName;
    $record->original = (int)$original;
    $record->timestamp = time();
    drupal_write_record('s3_image_storage', $record);

    // Expose a hook so other modules can react to the
    // creation of this derivative.
    module_invoke_all('s3_image_storage_new_record', $record);
  }

  /**
   * Generate a derivative for a single style.
   *
   * @param string $style_name
   *   Style Name.
   *
   * @throws Exception if style could not be loaded.
   * @throws Exception if derivative could not be generated.
   */
  private function generateDerivative($style_name) {
    // Load style.
    $style = image_style_load($style_name);
    if (!is_array($style)) {
      throw new S3ImageStorageImageProcessorException(sprintf('Style "%s" could not be loaded.', $style_name));
    }

    if (variable_get('s3_image_storage_preserve_originals', 0)) {
      // If originals should be retained, use Drupal functions to create derivatives.

      // Generate target path.
      $target_path = image_style_path($style_name, $this->uri);

      // Generate derivative.
      $success = image_style_create_derivative($style, $this->uri, $target_path);
      if (!$success) {
        throw new S3ImageStorageImageProcessorException(sprintf('Image derivative for style %s could not be generated at path %s.', $style_name, $target_path));
      } else {
        $this->syncFile($target_path, $this->determineObjectKey($style_name));
      }
    } else {
      // If originals should not be retained, use own function to apply images styles.

      // Generate derivative.
      $uri = $this->image_style_create_derivative($style, $this->uri);
      if ($uri) {
        // Upload to S3.
        $this->syncFile($uri, $this->determineObjectKey($style_name));

        // Delete temporary file.
        @drupal_unlink($uri);
      } else {
        throw new S3ImageStorageImageProcessorException(sprintf('Image derivative for style %s could not be generated at path %s.', $style_name, $uri));
      }
    }
  }

  /**
   * Determine the base directory for the current image URI.
   */
  private function determineBaseDirectory() {
    // Determine extension for path.
    $path_info = pathinfo($this->uri);
    $extension = (isset($path_info['extension'])) ? $path_info['extension'] : '';

    // Determine directory based on token.
    $directory = (!empty($this->baseDirectoryToken)) ? token_replace($this->baseDirectoryToken, array(
      'file' => file_load($this->fid),
    )) : 'image';

    // Set directory.
    $this->baseDirectory = $directory;
  }

  /**
   * Determine object key for image derivatives.
   *
   * @param string $filename Filename of image asset.
   * @return string $parts S3 object key.
   * @throws S3ImageStorageImageProcessorException
   */
  private function determineObjectKey($filename) {
    // Filename and base directory are required.
    if (empty($filename) || empty($this->baseDirectory)) {
      throw new S3ImageStorageImageProcessorException('Filename or base directory are not set.');
    }

    // Determine extension for path.
    $path_info = pathinfo($this->uri);
    $extension = (isset($path_info['extension'])) ? $path_info['extension'] : '';

    // Array of filenameparts.
    $parts = array(
      'directory' => $this->baseDirectory,
      'separator_style_name' => '/',
      '$filename' => $filename,
      'separator_extension' => '.',
      'extension' => $extension,
    );

    return implode($parts);
  }

  /**
   * Creates a new temporary image derivative based on an image style.
   * Taken from Drupal's Image module (image.module), but modified so that
   * image styles are only applied without storing the file in the filesystem.
   *
   * @param $style
   *   An image style array.
   * @param $source
   *   Path of the source file.
   *
   * @return bool
   *   TRUE if an image derivative was generated, or FALSE if the image derivative
   *   could not be generated.
   *
   */
  private function image_style_create_derivative($style, $source) {
    // If the source file doesn't exist, return FALSE without creating folders.
    if (!$image = image_load($source)) {
      return FALSE;
    }

    foreach ($style['effects'] as $effect) {
      image_effect_apply($image, $effect);
    }

    // Create temporary file.
    $destination = drupal_tempnam('temporary://', 's3_image_storage');

    if (!image_save($image, $destination)) {
      if (file_exists($destination)) {
        watchdog('image', 'Cached image file %destination already exists. There may be an issue with your rewrite configuration.', array('%destination' => $destination), WATCHDOG_ERROR);
      }
      return FALSE;
    }

    // Add correct file extension based on original file.
    $path_info = pathinfo($this->uri);
    $extension = (isset($path_info['extension'])) ? ('.' . $path_info['extension']) : '';
    $new_destination = $destination . $extension;
    rename($destination, $new_destination);

    return $new_destination;
  }
}
