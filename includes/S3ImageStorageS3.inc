<?php

/**
 * @file
 *
 * Class that manages integration with S3.
 */

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;

/**
 * Image processing class.
 */
class S3ImageStorageS3 {
  /**
   * @var array $s3Properties S3 connection properties.
   */
  protected $s3Props = array();

  /**
   * @var object $s3Client S3 Client.
   */
  protected $s3Client;

  /**
   * Constructor.
   */
  public function __construct() {
    // Determine S3 properties.
    $this->determineS3Properties();

    // Initialize client.
    $this->initializeS3Client();
  }

  /**
   * Determine properties required for S3 uploads.
   */
  private function determineS3Properties() {
    // Determine required properties.
    $this->s3Props['bucketName'] = variable_get('s3_image_storage_storage_bucket_name', '');
    $this->s3Props['objectAcl'] = variable_get('s3_image_storage_storage_acl', '');
    $this->s3Props['region'] = variable_get('s3_image_storage_storage_region', '');

    // S3 properties cannot be empty.
    array_walk($this->s3Props, function($property, $key) {
      if (empty($property)) {
        throw new Exception($key . ' is not valid: ' . $property);
      }
    });
  }

  /**
   * Get S3 props.
   *
   * @param string $key S3 Props Array key.
   * @return mixed Array or individual key.
   */
  public function getS3Props($key = '') {
    if (empty($key)) {
      return $this->s3Props;
    } else {
      return $this->s3Props[$key];
    }
  }

  /**
   * Get the location of a bucket. Really used as a test function.
   */
  public function getBucketLocation() {
    return $this->s3Client->getBucketLocation(array(
      'Bucket' => $this->s3Props['bucketName'],
    ));
  }

  /**
   * Initialize S3 client.
   *
   * Use access key id and secret access key from Drupal variables,
   * they should be stored in the settings file for this environment.
   *
   * @see http://docs.aws.amazon.com/aws-sdk-php/v3/guide/guide/credentials.html
   *   for alternative options on how to provide credentials.
   */
  private function initializeS3Client() {
    if (empty($this->s3Client)) {
      // Create default credentials interface.
      $credentials = new Credentials(
        variable_get('s3_image_storage_s3_bucket_access_key_id', ''),
        variable_get('s3_image_storage_s3_bucket_secret_access_key', '')
      );

      // Create a new S3 client.
      $this->s3Client = new S3Client(array(
        'version' => 'latest',
        'region' => $this->s3Props['region'],
        'credentials' => $credentials,
      ));
    }
  }
}
