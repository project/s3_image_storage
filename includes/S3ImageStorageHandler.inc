<?php

/**
 * Define custom exception class for handler class.
 */
class S3ImageStorageHandlerException extends Exception{}

/**
 * Image handler class.
 *
 * Handles queuing as well as sending images to S3 processor.
 */
class S3ImageStorageHandler{
  /**
   * @var array $metadata Metadata for image processing.
   */
  private $metadata = array();

  /**
   * @var object $file File object.
   */
  private $file;

  /**
   * @var string uri URI for this file. Separate from file key because
   *   key value is different on file insert.
   */
  private $uri = '';

  /**
   * Constructor.
   *
   * @param array $args Array of parameters including:
   *   - (string)   uri               URI
   *   - (object)   file              File Object.
   *   - (string)   field_name        Field name.
   *   - (bool)     regenerate        Optional flag indicating that the original
   *                                  image should be regenerated.
   *   - (bool)     force_process     Forcefully process the image, i.e.
   *                                  don't queue the image (again).
   *
   * @throws S3ImageStorageHandlerException.
   */
  public function __construct($args) {
    // Validate passed arguments.
    $this->validateArguments($args);

    // Set URI and regenerate flag.
    $this->uri = $args['uri'];
    $this->regenerate = (array_key_exists('regenerate', $args)) ? $args['regenerate'] : false;

    // Check for file.
    if (!is_object($args['file'])) {
      throw new S3ImageStorageHandlerException(sprintf('File object could not be loaded.'));
    }
    $this->file = $args['file'];

    // Determine metadata.
    $field_name = (array_key_exists('field_name', $args)) ? $args['field_name'] : null;
    $this->metadata = self::determineMetaData($this->file, $field_name);

    // Determine if any image styles should be generated for this image.
    if (empty($this->metadata['styles']) && !$this->metadata['upload_original']) {
      // Nothing to do for this file.
      return;
    }

    // Generate derivatives, either instantly or in Drupal queue.
    if ($this->metadata['queue_processing'] != 0 && (!array_key_exists('force_process', $args) || $args['force_process'] != true)) {
      // Then, queue image.
      $this->queueImage();
    } else {
      // Process instantly.
      $this->processImage();
    }
  }

  /**
   * Determine metadata.
   *
   * @param object $file File object to be checked.
   * @param string $field_name Field name. Required for a regular
   *   file load, i.e. when the file object does not contain a
   *   key for the source.
   * @return array $metadata Metadata.
   * @throws S3ImageStorageHandlerException.
   */
  public static function determineMetaData($file, $field_name = '') {
    // Ensure argument has valid type.
    if (!is_object($file)) {
      watchdog('s3_image_storage', 'Passed argument needs to be an object.', WATCHDOG_ERROR);
      return false;
    }

    // Check against valid mime type.
    $valid_types = array('image/jpeg', 'image/gif', 'image/png');
    if (!property_exists($file, 'filemime') || !in_array($file->filemime, $valid_types)) {
      throw new S3ImageStorageHandlerException(sprintf('File type is not supported.'));
    }

    // Setup array for metadata.
    $metadata = array(
      'styles' => array(),
      'field_name' => '',
    );

    // Find correct field name from field list unless it is passed as an argument.
    if (empty($field_name)) {
      // Remove last 2 elements of source, which contains language and index.
      $source_parts = explode('_', $file->source);
      $source_parts = array_slice($source_parts, 0, -2);
      $metadata['field_name'] = implode('_', $source_parts);
    } else {
      $metadata['field_name'] = $field_name;
    }

    // Load field.
    $field = field_info_field($metadata['field_name']);
    if (empty($field)) {
      throw new S3ImageStorageHandlerException(t('Detected field (%field_name) is not a valid field.', array('%field_name' => $metadata['field_name'])));
    }

    // Determine styles for this field. Only return values that are not empty.
    if (!empty($field['settings']['s3_image_storage']['image_styles'])) {
      $metadata['styles'] = array_filter($field['settings']['s3_image_storage']['image_styles'], function($value) {
        return (!empty($value));
      });
    }

    // Determine if images for this field should be processed using queues.
    if (isset($field['settings']['s3_image_storage']['queue_processing'])) {
      $metadata['queue_processing'] = $field['settings']['s3_image_storage']['queue_processing'];
    } else {
      $metadata['queue_processing'] = 0;
    }

    // Determine if original image should be uploaded.
    if (isset($field['settings']['s3_image_storage']['upload_original'])) {
      $metadata['upload_original'] = (bool)$field['settings']['s3_image_storage']['upload_original'];
    } else {
      $metadata['upload_original'] = false;
    }

    // Determine directory token for this field.
    if (!empty($field['settings']['s3_image_storage']['base_directory_token'])) {
      $metadata['base_directory_token'] = $field['settings']['s3_image_storage']['base_directory_token'];
    }

    // Return styles.
    return $metadata;
  }

  /**
   * Queue image for later processing.
   */
  private function queueImage() {
    // Process on queue run.
    $queue = DrupalQueue::get(S3_IMAGE_STORAGE_IMAGE_PROCESSING_QUEUE);

    // Attempt to queue the item.
    $success = $queue->createItem(array(
      'fid' => $this->file->fid,
      'uri' => $this->uri,
      'field_name' => $this->metadata['field_name'],
      'regenerate' => $this->regenerate,
    ));

    // Add logging.
    if ($success) {
      watchdog('s3_image_storage', 'Queued image for processing at URI !uri', array('!uri' => $this->uri), WATCHDOG_INFO);

      // Attempt to run the image cron instantly depending on settings.
      if ($this->metadata['queue_processing'] == 2) {
        new S3ImageStorageCron();
      }
    } else {
      watchdog('s3_image_storage', 'Queuing failed for URI !uri', array('!uri' => $this->uri));
    }
  }

  /**
   * Process image instantly.
   */
  private function processImage() {
    // Determine if image needs to be deleted first.
    if ($this->regenerate === true) {
      watchdog('s3_image_storage', 'File (fid = !fid) was scheduled for regeneration.', array(
        '!fid' => $this->file->fid,
      ));

      // Delete image from S3.
      $deletedMetadata = s3_image_storage_delete_file($this->file);
    }

    // Generate derivatives and upload.
    try {
      $imageProcessor = new S3ImageStorageImageProcessor(array(
        'fid' => $this->file->fid,
        'uri' => $this->uri,
        'styles' => $this->metadata['styles'],
        'base_directory_token' => $this->metadata['base_directory_token'],
        'field_name' => $this->metadata['field_name'],
        'regenerate_data' => ($this->regenerate) ? $deletedMetadata : array(),
      ));

      // Upload original.
      if ($this->metadata['upload_original']) {
        $imageProcessor->uploadOriginal();
      }

      // Generate and upload derivatives.
      $imageProcessor->generateDerivatives();
    } catch (Exception $e) {
      watchdog_exception('s3_image_storage', $e);
      drupal_set_message('Image uploads to S3 failed. Please check your logs for more details.', 'error');
    }
  }


  /**
   * Validate passed arguments. Required keys cannot be empty.
   *
   * @param array $args Arguments.
   * @throws S3ImageStorageHandlerException.
   */
  private function validateArguments($args) {
    if (!is_array($args)) {
      throw new S3ImageStorageHandlerException(sprintf('Arguments need to be an array.'));
    }

    // Find required keys and ensure they are not empty.
    $empty_required_keys = array();
    array_walk($args, function($value, $key) use (&$empty_required_keys) {
      $required_keys = drupal_map_assoc(array('file', 'uri'));
      if (in_array($key, $required_keys)) {
        if (empty($value)) {
          $empty_required_keys[$key] = $value;
        }
      }
    });

    if (!empty($empty_required_keys)) {
      throw new S3ImageStorageHandlerException(sprintf('One or many required arguments are missing.'));
    }
  }
}
