<?php

/**
 * @file
 *
 * Class to upload files to S3 permanent storage.
 */
class S3ImageStorageS3Upload extends S3ImageStorageS3{
  /**
   * @var string uri File URI.
   */
  private $uri = '';

  /**
   * @var string $objectKey Full S3 object key for upload.
   */
  private $objectKey = '';

  /**
   * Constructor.
   * @param string $uri URI for file upload.
   * @param string $objectKey Full S3 object key.
   * @throws Exception
   */
  public function __construct($uri, $objectKey) {
    if (empty($uri) && !is_string($uri)) {
      throw new Exception('Passed URI is not a string');
    }

    // Initialize parent.
    parent::__construct();

    // Set URI for upload.
    $this->uri = $uri;

    // Set object key.
    $this->objectKey = $objectKey;
  }

  /**
   * Generic function to upload file.
   */
  public function uploadFile() {
    // Upload file.
    return $this->s3Client->putObject([
      'ACL' => $this->s3Props['objectAcl'],
      'Bucket' => $this->s3Props['bucketName'],
      'Key'    => $this->objectKey,
      'Body' => file_get_contents($this->uri),
      'ContentType' => file_get_mimetype($this->uri),
    ]);
  }
}
