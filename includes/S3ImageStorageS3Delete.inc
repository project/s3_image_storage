<?php

/**
 * @file
 *
 * Class to delete files from S3 permanent storage.
 */
class S3ImageStorageS3Delete extends S3ImageStorageS3{
  /**
   * @var object $file File object.
   */
  private $file;

  /**
   * @var array $records S3 records for this file.
   */
  private $records = array();

  /**
   * @var bool $preserveRemoteOriginal Flag indicating if remote original
   *   should be preserved when local file gets deleted.
   */
  private $preserveRemoteOriginal = false;

  /**
   * @var bool $preserveRemoteDerivatives Flag indicating if remote derivatives
   *   should be preserved when local file gets deleted.
   */
  private $preserveRemoteDerivatives = false;

  /**
   * @var string $baseDirectory Previously used base directory for image uploads.
   */
  private $baseDirectory = '';

  /**
   * @var stering $bucket Bucket user for files.
   */
  private $bucketName = '';

  /**
   * Constructor.
   * @param obj $file URI for file upload.
   * @throws Exception
   */
  public function __construct($file) {
    if (!is_object($file) || !property_exists($file, 'fid')) {
      throw new Exception('File is not valid.');
    }

    // Store variable.
    $this->file = $file;

    // Get parameters.
    $this->preserveRemoteOriginal = (boolean)variable_get('s3_image_storage_preserve_remote_original', 0);
    $this->preserveRemoteDerivatives = (boolean)variable_get('s3_image_storage_preserve_remote_derivatives', 0);

    // If original and derivatives should be kept on file deletion,
    // don't load records.
    if ($this->preserveRemoteDerivatives && $this->preserveRemoteOriginal) {
      return;
    }

    // Get records for this file.
    $this->getRecords();

    // Determine base directory.
    $this->determineBaseDirectory();
  }

  /**
   * Load all S3 records for this file.
   */
  private function getRecords() {
    // Load all entries for this file record.
    $query = db_select('s3_image_storage', 's')
      ->fields('s')
      ->condition('s.fid', $this->file->fid);

    // If the remote original should be preserved,
    // exclude it from the list of records.
    if ($this->preserveRemoteOriginal) {
      $query->condition('s.original', 1, '!=');
    }

    // If any remote derivatives should be preserved,
    // exclude them from the list of records.
    if ($this->preserveRemoteDerivatives) {
      $query->condition('s.original', 1);
    }

    // Get results.
    $results = $query->execute();

    // Convert records into array.
    foreach ($results as $record) {
      $this->records[] = $record;
    }
  }

  /**
   * Determine the base directory
   */
  private function determineBaseDirectory() {
    // Only proceed if records exist.
    if (empty($this->records)) {
      return false;
    }

    // Use the object key of the first record.
    $objectKey = $this->records[0]->object_key;

    // Determine the base directory.
    $this->baseDirectory = substr($objectKey, 0, strrpos($objectKey, '/'));
  }

  /**
   * Determine bucket name based on local records.
   * Assuming that all derivatives of a single file have been uploaded
   * to the same bucket. Pick the bucket name from the first record.
   */
  private function determineBucketName() {
    $firstRecord = $this->records[0];
    $this->bucketName = $firstRecord->bucket_name;
  }

  /**
   * Get the base directory for this remote file.
   */
  public function getBaseDirectory() {
    return $this->baseDirectory;
  }

  /**
   * Delete local records and remote images for this file.
   */
  public function cleanupFile() {
    // Only proceed if records exist.
    if (empty($this->records)) {
      return false;
    }

    // Delete local records.
    $this->removeLocalRecords();

    // Delete remote images.
    $this->deleteRemoteImages();
  }

  /**
   * Remove local records.
   */
  private function removeLocalRecords() {
    // Get IDs that need to be cleaned up.
    $ids = array_map(function($record) {
      return $record->id;
    }, $this->records);

    $num_deleted = db_delete('s3_image_storage')
      ->condition('id', $ids, 'IN')
      ->execute();

    watchdog('s3_image_storage', t('Delete !count local records for file id !fid.', array(
      '!count' => $num_deleted,
      '!fid' => $this->file->fid,
    )));
  }

  /**
   * Delete remote images for this file.
   *
   * @return object $result AWS result set.
   */
  private function deleteRemoteImages() {
    // Initialize parent.
    parent::__construct();

    // Determine bucket name.
    $this->determineBucketName();

    // Convert objects into correct format for SDK.
    $objects = array();
    foreach ($this->records as $record) {
      $objects[] = array(
        'Key' => $record->object_key,
      );
    }

    // Delete file from S3.
    // Even if one, multiple, or all files have already been deleted from S3,
    // the request will be successful.
    $result = $this->s3Client->deleteObjects([
      'Bucket' => $this->bucketName,
      'Delete' => array(
        'Objects' => $objects,
      ),
    ]);

    watchdog('s3_image_storage', t('Delete !count remote files for file id !fid.', array(
      '!count' => count($result['Deleted']),
      '!fid' => $this->file->fid,
    )));

    // Determine if there were any errors deleting the remote objects.
    $errors = $result->get('Errors');

    // Show a visible message.
    if (!empty($errors)) {
      foreach ($errors as $error) {
        watchdog('s3_image_storage', 'Could not delete the object @object_key due
        to the following error code: @error_code.', array(
          '@object_key' => $error['Key'],
          '@error_code' => $error['Code'],
        ), WATCHDOG_ERROR);
      }
      drupal_set_message('There was an error deleting one or multiple remote
        objects related to this S3 object. Please refer to the log for
        details.', 'error', false);
    }

    return $result;
  }
}
